import csv
import pathlib
from prettytable import PrettyTable, from_csv

cur_dir_prefix = str(pathlib.Path(__file__).parent.absolute()) + '/'

text_table = PrettyTable()
lst = list()
lst2 = list()
with open(cur_dir_prefix + 'pt_1.csv', 'r') as infile:
    reader = csv.reader(infile)


    headers = next(reader)
    text_table.field_names = headers

    
    for idx,row in enumerate(reader):
        if len(row) == len(headers):
            text_table.add_row(row)
            lst.append(row)
        else:
            print(f'skipping row {idx} - {row}')
    print(text_table)

# Title,Name,ID,Email,Company,Updated
# Updated, email ID, Title,Name,Company
# 05/11/2018,diam.Duis.mi@fringillapurus.net 16200816-7450,,"Hensley, Martina G.",Nonummy Consulting


with open(cur_dir_prefix + 'pt_2.csv', 'r') as infile:
    reader = csv.reader(infile)
    lst2 = list()
    for idx,row in enumerate(reader):
        if len(row) == len(headers) - 1:
            rearr_row = [row[2], row[3], row[1].split()[1],
                        row[1].split()[0], row[4],
                        row[0]]
            text_table.add_row(rearr_row)
            lst2.append(rearr_row)
        else:
            print(f'skipping row {idx} - {row}')
    print(text_table)

lst.extend(lst2)
lst.sort(key=lambda x: x[-1].split('/')[::-1])

with open(cur_dir_prefix + 'pt_1_sorted.csv', 'w', encoding='utf8') as f:
    writer = csv.writer(f)
    for row in lst:
        writer.writerow(row)

