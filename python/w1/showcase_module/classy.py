class MyBaseClass(object):
    some_static_int = 10
    
    def __init__(self, some_text: str):
        self.text = some_text
    
    # note the 'self' parameter to associate this with the object
    def slazzy_print(self):
        print(f'{self.some_static_int} - {self.text}')
    
    @staticmethod
    def static_method():
        print(f'I don\'t belong to a class or object, just share the namespace')
    
    @classmethod
    def class_method(cls):
        print(f'I belong to the class {cls.__name__}')

class SubClass(MyBaseClass):
    def __init__(self, some_text: str, some_more_text: str):
        super().__init__(some_text)
        self.more_text = some_more_text
    
    def slazzy_print(self):
        super().slazzy_print()
        print(f'{self.more_text}')

