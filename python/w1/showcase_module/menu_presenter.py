from typing import Dict, List

## assign auto idx values to given menu list, starting from
## note that ': list' and ' -> int' hints are optional
#  @param menu_list List of menu items
#  @oaram auto_zero_prompt If a menu entry with idx '0' should be printed automatically
#  @param auto_zero_text The text to follow the value-0, if auto_zero_prompt is False, 
#  this is ignored
#  @return entered idx, -1 for invalid
def menu_selector_w_auto_indexing(menu_list: List[str], auto_zero_prompt=True, 
                                  auto_zero_text='Exit') -> int:
    menu_dict = dict(zip(range(1, len(menu_list) + 1), menu_list))
    return menu_selector(menu_dict, auto_zero_prompt, auto_zero_text)

## use menu_dict keys as idx and values as the prompt text
## note that ': list' and ' -> int' hints are optional
#  @param menu_dict dictionary with idx - text key,value pairs
#  @param auto_zero_prompt If a menu entry with idx '0' should be printed automatically
#  @param auto_zero_text The text to follow the value-0, if auto_zero_prompt is False, 
#  this is ignored
#  @return entered idx, -1 for invalid
def menu_selector(menu_dict: Dict[int, str], auto_zero_prompt=True, auto_zero_text='Exit') -> int:
    selection = -1
    
    if len(menu_dict) > 0:
        for idx, menu_item in menu_dict.items():
            print(f'{idx:3} - {menu_item} ')
        if True == auto_zero_prompt:
            print(f'{0:3} - {auto_zero_text}')
        try:
            selection = int(input(('\nEnter selection: ')))
        except ValueError:
            # just swallow the exception and let the caller handle '-1'
            selection = -1

        if selection not in menu_dict:
            if False == auto_zero_prompt:
                selection = -1
            elif selection != 0:
                selection = -1
        
    return selection

if __name__ == '__main__':
    print(menu_selector_w_auto_indexing(['aa', 'bbb', 'ccc']))
    print(menu_selector({10: 'xx', 25: 'qq'}))
