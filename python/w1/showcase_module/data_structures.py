import collections

def show_me_the_strings(input_str='The quick brown fox jumps over the lazy dog'):
    print(f'all capital: {input_str.upper()}') # lower to lowercase
    print(f'all title: {input_str.title()}')
    print(f'char count: {len(input_str)}, word count: {len(input_str.split())}')
    print(f'1st: {input_str[0]}, last: {input_str[-1]}, 1st-2nd: {input_str[0:2]}')
    print(f'{input_str.split()} {input_str.split(input_str.split()[2])}')

def lists():
    my_list_1 = list()
    # or, we can use [] to create a new instance too
    my_list_1 = []
    my_list_1.extend([1, 3, 5, 1]) # add elements individually
    my_list_1.append([7, 9, 1]) # add the blob as it is
    print(my_list_1)
    print(1 in my_list_1)
    print(f'{my_list_1.count(1)}')
    print(my_list_1.pop(3))
    my_list_1.pop()
    print(f'{my_list_1}')
    my_list_1.reverse() # in-place reversal
    print(f'{my_list_1} - idx of 5: {my_list_1.index(5)}')
     # reverse=False -> (optional) sorting order, default is False, 
     # key=myFunc -> (optional) sorting function
    my_list_1.sort(reverse=False) # in-place sorting
    print(f'{my_list_1}')
    # other list methods clear, copy
    
def tuples():
    just_a_tpl = (1, 'aa', 3.8)
    # fields are indexed by []s
    print(f'{just_a_tpl[0]}')
    
    # first param is the name of the named tuple, the second is list of tuple fields
    Person = collections.namedtuple('Person', 'name age gender children')
    jane = Person(name='Jane', age=30, gender='f', children=['Jack', 'Jill'])
    # fields are indexed by name
    print(f'{jane.name}')
    # the below won't work as tuples are immutable
    # jane.name='Alice'
    
    # tuple itself is immutable but any object members can be modified
    jane.children.append('Marcus')
    print(f'{jane.children}')
    
def set_the_sets():
    lst = [5,7,2,7,4,1]
    # set only contains unique elements, duplicates are dropped automatically
    # it isn't ordered automatically
    my_set = set([5,7,2,7,4,1])
    print(f'{len(lst)} vs {len(my_set)}, {my_set}')
    
def dictionaries():
    my_dct = dict()
    # access the slot by [], either to update an existing value or add a new key-value pair, kvp
    my_dct[10] = 'a'
    # bulk-add a kvp group
    my_dct.update({30: 'c', 20: 'b'})
    
    sorted_by_val_dict = {k: v for k, v in sorted(my_dct.items(), key=lambda item: item[1], reverse=False)}
    [my_dct.pop(key) for key in [20, 10]]
    print(f'{my_dct} - {sorted_by_val_dict}')
    
    if 50 in my_dct:
        print(f'we\'ve got 50')
    
    # default dictionaries assign a default value to non-existent keys, so, accessing it won't raise an exception
    # the default value depends on the type of the defaultdict
    # also, once a non-existent key is accessed, it is automatically added to the dictionary  
    def_dct = collections.defaultdict(int)
    print(f'{def_dct[50]} - {def_dct.keys()}')
    
def iterators():
    # generate an iterator [0 - 11| by increments of 2
    rng = range(0,11, 2)
    print(f'{rng} - {list(rng)}')
    # iterator types yield 'iterators' which generate the values lazily, as requested
    # to see the conent, we can convert the iterator to a list for example
    # zip operatons on the min(len(ds1), len(ds2)) length
    print(f"{list(zip([1, 2, 3, 4, 5], ['a', 'b', 'c']))}")
    print(f'{list(enumerate([3,5,7]))}')
    # enumerate 'zip's sequential numeric indexes with the elements in the passed-in data structure
    for idx, val in enumerate([3,5,7]):
        print(f'{idx} - {val}')
    
    
if __name__ == '__main__':
    show_me_the_strings()
    lists()
    tuples()
    set_the_sets()
    dictionaries()
    iterators()