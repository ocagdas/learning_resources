#!/usr/bin/env python3
# coding: utf-8

import numpy as np
import pandas as pd
from sklearn.datasets import load_iris
from sklearn import tree
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
#from IPython.display import Image
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder
from sklearn.compose import ColumnTransformer
from sklearn import metrics
from sklearn.model_selection import GridSearchCV
import graphviz

df_3_3 = pd.read_csv('3_3_train.csv', index_col='ID')
df_3_3_test = pd.read_csv('3_3_test.csv', index_col='ID')

df_list = []
for df_cur in [df_3_3, df_3_3_test]:
    # shorten attribute names
    df_cur = df_cur.rename(columns={'Age of Driver': 'AoD', 'Age of Licence': 'AoL',
                                     'Age of Policy': 'AoP', 'Unspent Convictions': 'UC',
                                     '1st Party Claims': '1PC', '3rd Party Claims': '3PC',
                                     'No Claims Protection': 'NCP','No Claims': 'NC',
                                      'Exp. Driver': 'ED', 'Good Driver': 'GD'})

    aod_bins = [0, 24, 74, float("inf")]
    aod_names = [0, 1, 2]

    # we can modify the cells to test
    #df_3_3_nonan.loc[0,'AoD'] = 75

    df_cur['AoD'] = pd.cut(df_cur['AoD'], aod_bins, labels=aod_names, include_lowest=True)

    aol_bins = [0, 3, float("inf")]
    aol_names = [0, 1]

    df_cur['AoL'] = pd.cut(df_cur['AoL'], aol_bins, labels=aol_names, include_lowest=True)

    aop_bins = [-float("inf"), 0, float("inf")]
    aop_names = [0, 1]
    
    df_cur['AoP'] = pd.cut(df_cur['AoP'], aop_bins, labels=aop_names, include_lowest=True)
    
    # not sure if this really helps but set type as 'category'
    for subj in df_cur.columns.tolist():
        df_cur[subj] = df_cur[subj].astype('category')

    df_cur = df_cur.dropna(axis='columns')
    # convert N/Y cols to 0/1
    for subj in ['UC', 'NCP', 'NC', 'ED', 'GD']:
        df_cur = df_cur.replace({subj: 
                    {'N': 0, 'Y': 1, np.nan: np.nan}})
    
    df_list.append(df_cur)
    

#print(df_list[0])
#print(df_list[1])


df_x = df_list[0].loc[:, 'AoD' : 'NCP']
df_y = df_list[0].loc[:, 'NC']
# can disable shuffle for consistent results
X_train, X_test, y_train, y_test = train_test_split(
    df_x, df_y, test_size=0.30, shuffle=True)

#enc = OneHotEncoder(handle_unknown='ignore')
#print(pd.DataFrame(enc.fit_transform(X_train[['AoD']]).toarray()))

#X_train = pd.get_dummies(X_train, columns=["AoD"], prefix=["AoD"])
#X_test = pd.get_dummies(X_test, columns=["AoD"], prefix=["AoD"])

clf = tree.DecisionTreeClassifier()
clf = clf.fit(X_train, y_train)
print(y_test)
y_pred = clf.predict(X_test)
# print('Accuracy:', metrics.accuracy_score(y_test, y_pred))
print('clf score', clf.score(X_test, y_test))

plt.clf()
plt.figure(figsize=(10,10))
tree.plot_tree(clf, filled=True, feature_names=X_train.columns[:], class_names=['0', '1'])
plt.title("Decision trees")
#plt.show()

print('Starting gridsearch, be patient...')
parameters = {'max_depth':range(3,20)}
clf = GridSearchCV(tree.DecisionTreeClassifier(), parameters, n_jobs=6)
clf = clf.fit(X_train, y_train)
print('GridSearchCV clf score', clf.score(X_test, y_test))
y_true, y_pred = y_test, clf.predict(X_test)
print(metrics.classification_report(y_true, y_pred))
tree_model = clf.best_estimator_
print (clf.best_score_, clf.best_params_)
print('end gridsearch')

dot_data = tree.export_graphviz(clf.best_estimator_, out_file=None, 
            filled=True, feature_names=X_train.columns[:], class_names=['0', '1'])
graph = graphviz.Source(dot_data)
graph.render('dtree_render_Decision trees',view=True)
print('finito')
