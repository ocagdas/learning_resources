// this is where we 'import' other packages to use functionality built by others
// we have plenty of 'java.' packages which are provided by the Java Virtual Machine, JVM,
// supplier, so, we don't have to do everything ourselves
import java.util.ArrayList;
import java.util.Scanner;

// 'public' classes live in matching files
// so HelloWorld should be in HelloWorld.java
// when you 'compile' a java file, it generates a '.class' file for you
// this is the 'byte code' to be executed by the JVM and do not confuse it with
// the 'Object Oriented' 'Class' concept
// also, whatever you write after '//'s are comments and compiler won't give a toss
/*
 * 
 * If you want to write block comments, you can also use this structure 
 *
 *
 */
// Below  is the class declaration, tells us the 'name' and 'visibility' of the class
// Visibility only matters when you have bunch of classes, package them and don't want
// others to see some of your precious, secret classes
// Also, the 'styling' of the code is just for your eyes, not for java
// I mean, if/how you indent your code, if the { goes on the same line or next,
// this is all for you. Having said that, do not forget ';' at the end of lines
// that don't belong to a {} block.
// Always match your ()s and {}s and keep an eye on red wiggly underline on non-comment
// lines where Eclipse is telling you that there is something wrong with your code.
public class HelloWorld {
	// welcome to the class body, this is where we declare 
	// member variables and methods for the class
	
	// this is an 'int' variable, it can store 'whole' numbers,
	// no fiddly decimals allowed
	// as it is in the 'class body', it is a 'member variable' of the class
	// so, every 'instance' or 'object' of this class gets its own copy of members
	// not finished yet, this 'int' is a 'primitive type, it's not a fancy class
	// it has no methods, just a memory location to store some value.
	// we have a handful of 'primitive types' to store numbers. They all
	// start with lowercase.
	int memberInt;
	
	// another member variable, this time it's double', which means, it allows decimals
	// you don't have to but you can assign a 'default' value to a member variable where it 
	// is 'declared'. This is called 'initialisation'. This value can change later. 
	double memberDbl = 1.01;
	
	// this line below won't compile because the right hand side is a character array, 
	// or string, and the variable type is 'int', whole number, as java is 'strongly typed'
	// we can't assign incompatible types to each other
	// int justint = "hello hello";
	
	// but we can assing a 'char' to an 'int'. both are whole numbers and 'char' holds
	// smaller numbers than 'int', so, it's like putting toddler shoes in adult shoe box
	// and that's why it is allowed but it won't work other way around, at least automatically
	char chrVar = 5;
	int intVar = chrVar;
	
	// decimals are 'double' by default so, you need an 'f' to say we want this decimal
	// to be a float, which is a less precise decimal type
	// if you have some constants which won't change during the program, you can make them
	// 'final'. Then, it won't even compile if you try to change them later.
	// If you don't initialise the 'final' in the 'class body', the only place you can
	// do it is in the 'constructor'
	final float myShortPi = 3.14f;
	
	// another integer, but look how it is spelt and the initial is capitalised.
	// That's because this is a 'wrapper class'. It takes a poor 'primitive' and
	// 'wraps' it in an 'object'. Why? So we can do bunch of operations on a simple
	// number. Also, this comes handy when we use 'collections'.
	Integer intObj = 3;
	String instanceName = "DefaultName";
	
	// we'll explain the 'static' keyword later but basically, static members
	// are shared between all instances of the class, if one of them modifies it
	// the other sees it. We even don't need an object to access the 'static' variables
	// they can be accessed as ClassName.staticVarName, ie HelloWorldd.staticStr
	static String staticStr = "ClassString";
	
	// This is the entry point of the program, every Java program has only one
	// public static void main(String[] args) method
	// if we define what's what loosely
	// public: anybody who can see this class, can see this method/variable
	// static: this can be called even without an object
	// void: this method returns nothing
	// main: name of the method
	// String[] args: If you pass any parameters to the program
	// they are conveyed here as an 'array of arguments'.
	// For example HelloWorld --setTemp 20 --burglarAlarm on
	public static void main(String[] args) {
		// This is a print statement that writes to 'standard out'
		// It is not graphical, it goes on the console as text
		System.out.println("Hello World! Let's crack on!");
		
		// first, experiment with some local variables. These variables just live in the 'main'
		// method, every method's locals just live there and aren't visible to others.
		// as j and k primitive variables, assignment copies the value of 'k' into 'j'
		int k = 5;
		int j = k;
		System.out.println("before j: " + j + " and k: " + k);
		j = 15;
		k = 20;
		// then, modifying either of them won't affect the other because they are independent
		System.out.println("after j: " + j + " and k: " + k);
		
		// We are in a static method which doesn't need an 'instance'
		// Let's create some 'instances' or 'objects' of HelloWorld 'class'
		// to do that we say 'new' to 'instantiate' the class and we tell which
		// 'constructor' to use
		
		// create a new instance via the 'default constructor' or 'ctor'
		// and assign it to a 'reference' called viaDefaultCtor
		HelloWorld viaDefaultCtor = new HelloWorld();
		
		// Another instance, this time via a 'ctor' with an argument
		HelloWorld viaArgCtor = new HelloWorld("Cat");
		// Now, we have 2 distinct 'objects' or 'instances' of the HelloWorld Class
		
		// constructor is a special method, we can't' do HelloWorld.HelloWorld()
		// it can only be accessed via 'new'
		
		// 'justInst' is just an 'object reference' or 'reference'
		// see, there is no 'new' or mention of a 'ctor'
		// it just points to an existing object.
		// If we use the 'class is a blueprint' analogy, every time
		// you say 'new' a new house is built, which is your 'object'/'instance'.
		// this house has an address too but these builders are busy, when they are
		// done building the house, they go away. So, to remember where this 'house' is
		// we store its address in a 'reference', ie viaArgCtor. So, the reference is not
		// the house but it takes us to it.
		// There can be multiple references to the same house. I save it as 'home', someone else
		// saves it as 'besties_house' but they all take us to the same house.
		// Also, when you move houses, you can keep your 'home' variable but 
		// make it 'reference' your new house
		HelloWorld justInst = viaArgCtor;
		// Now, we still have 2 'objects' of HelloWorld but 3 references, 2 of which
		// point at the same object
		
		// To access 'methods' or 'members' of an object, we add a '.', 
		// then, type the method name and pass the parameters in brackets,
		// separated with ','s if more than 1.
		// Methods can have 0, 1 or up to 255 parameters.
		// If you define more than 3 though, clean-coding fairies might sting you 
		// When the 'callee', the function we just called, completes,
		// we return back to here and continue from the 
		// next execution point, we can say next line for now
		// Now go and have a look at the 'testMethod'
		// see how viaArgCtor and justInst print the same instance name, 'Cat'
		// because, both variables are 'references' to the same 'object'
		// viaDefaultCtor though, is a reference to another instance/object
		// of the same class. So, making a modification through 'viaArgCtor' is also
		// visible when you access the members via 'justInst'. Again, both references
		// point at the same object.
		// every time you see '.testMethod' please go to 'testMethod' and then com back to
		// next line when you are done there
		viaDefaultCtor.testMethod("MoNeY");
		viaArgCtor.testMethod("more MoNeY");
		justInst.testMethod("MoNeY MoNeY MoNeY");
		
		// OK, some boring CS stuff. A running program maintains a 
		// Program Counter(PC), to remember which line it is going to execute next
		// There is also a thing called 'call stack'. Every time you call a method, 
		// the current PC and some other things are backed up, parameters for 
		// the 'callee' are prepared, a new 'frame', like a fresh page to scrible on, 
		// is allocated for the 'callee to allocate its local variables on,
		// we jump into the new function and when its done,
		// return value, if there is any, is copied, stack frame is thrown away
		// and the PC, like a bookmark, is restored and the execution starts
		// from where it was left in the calling function
		
		// To call a method, we need an object to reference through
		// but not always, that's when the method is 'static'. 'static' makes 
		// a 'method' or 'member variable' to belong to the class.
		// Those can be accessed even without an object, if one object
		// modifies a 'static' member, others see it too
		// static methods/members can be accessed via the Class Name or 
		// an object reference, so, both
		HelloWorld.staticTestMethod();
		//and
		k = justInst.staticTestMethod();
		// are valid but the latter is considered less nice
		// No matter how, if you moodify a 'static member' of a 'Class', it will
		// be visible to all 'instances' of the 'Class', as well as when the 
		// 'static member' is accessed directly through the class itself
		// also note how we stored the value returned by a method, staticTestMethod, 
		// in a variable, 'k'
		
		// now, let's see what happens when we pass a primitive variable as an
		// argument to a method
		System.out.println("k before mehod call: " + k);
		viaArgCtor.testMethodWithPrimitiveArg(k);
		System.out.println("k after mehod call: " + k);
		// you should have observed that the value of 'k' remained the same. But how?
		// 'primitive' arguments are copied when passed into methods as parameters and
		// we end up having 2 independent variables. Changing one won't affect the other.
		
		// see how we modify a static member via one instance and how
		// it is visible to the other instance
		justInst.staticStr = "newVal";
		System.out.println("static String: " + viaDefaultCtor.staticStr);
		
		// So, do you then notice that System.out.println is actually a 'static' method
		// call on the System class?
		
		// See how modifying member variable of one object does not
		// modify the member of the other object. That's because,
		// member variables do belong to the object, like, two houses,
		// both with front doors with same properties but they all have their own door
		justInst.memberInt = 3;
		viaDefaultCtor.memberInt = 5;
		System.out.println("memberInt from 2 instances: " + 
				justInst.memberInt + " and " + viaDefaultCtor.memberInt);
		
		// This bit here might look a bit confusing, so, please concentrate and read
		// carefully, even repeat once more
		// Now, remember, how we made 'justInst' point at the same object as 'viaArgCtor'
		// So, here, we again see that they both have the same name but 'viaDefaultCtor'
		// has a different one
		// When we pass 'viaDefaultCtor' as a parameter, we pass a copy of the reference
		// so, when/if the 'callee' modifies the object, that will be visible here too
		System.out.println("Before: Name of viaArgCtor: " + viaArgCtor.instanceName + 
				"justInst: " + justInst.instanceName + 
				"viaDefaultCtor: " + viaDefaultCtor.instanceName);
		
		HelloWorld.testMethodWithReferenceArg(viaDefaultCtor);
		
		// After returning from the method, viaArgCtor and justInst still show the same name 
		// because, they are references to the same object (same house saved with different 
		// names on different SatNavs) and the 'callee' didn't modify that object
		// However, we see that the object referenced by viaDefaultCtor has changed as the 'callee'
		// modified it. Also note that this modification didn't affect the other 'instances'
		// because, the modified field is an 'instance variable' which is unique to each instance. 
		System.out.println("After: Name of viaArgCtor: " + viaArgCtor.instanceName + 
				"justInst: " + justInst.instanceName + 
				"viaDefaultCtor: " + viaDefaultCtor.instanceName);
		
		HelloWorld.someLoops();
		HelloWorld.someConditionalOps();
		HelloWorld.someMaths();
		HelloWorld.someCollections();
		HelloWorld.someKeyboardInput();
	}
	
	// 'constructor' or 'ctor' is a special method that gets called
	// when a class is being instantiated. It cannot be called wit the '.'
	// notation as other methods.
	// If no other 'ctor' is defined, you get this 'no parameter'
	// 'default ctor' created automagically for you. Automagically means,
	// you don't see it but when you do new HelloWorld(), it's there for you.
	// However, the moment you declare any 'ctor', even just one, 
	// the 'default ctor' disappears
	HelloWorld() {
		
	}
	
	// This is a custom 'ctor'. Also note that 'ctor's don't have a return type
	// because they don't return anything.
	HelloWorld(String nameIn) {
		// assing the parameter to an instance variable, instances variables
		// are unique to each instance and this assignment won't affect
		// other instances
		// also once the variable is defined once, we don't specify the type
		// every time we assign a value to it. If you put the type, ie 'int', again
		// it will try to create a new varible
		instanceName = nameIn;
	}
	
	public void testMethod(String extraStr) {
		// this is a 'local' variable, it is created when we get into this function
		// and dies when we return
		// Unlike member variables, locals do not get initialised with default values
		// automatically, so, we have to initialise them
		String internalStr = extraStr;
		
		// do you see how we can break a long line into multiple lines
		// also internalStr is a copy of extraStr parameter which is passed in
		// by the caller, so, it changes 'dynamically'
		System.out.println("I'm " + instanceName + "! Thanks for giving me " + 
				internalStr);
		
		// return is optional for 'void' methods and see how it's
		// followed by a ';' immediately
		return;
		// return back to where you came from, please
	}
	
	public void testMethodWithPrimitiveArg(int inputArg) {
		// let's change the value of 'primitive' local variable 'inputArg'
		inputArg = inputArg + 5;
		
		// see how we omitted 'return' at the end of 'void' method
	}
	
	public static int staticTestMethod() {
		System.out.println("I'm static! I'm here even when there is no object around" + 
				" but you can call me via objects too");
		
		// this method 'has to' return an 'int' value
		return 3;
	}
	
	public static void testMethodWithReferenceArg(HelloWorld inputArgReference) {
		// now, we have a 'reference' to an object, a reference to an object
		// that is accessible from where this method is called too.
		// so, if we modify that object here, it will be visible through
		// other references to the same object as well
		inputArgReference.instanceName = "John";
	}
	
	public static void someLoops() {
		// well, when you have to repeat the same or very similar operation(s)
		// luckily you don't have to copy and paste the same line multiple times
		// we have various 'loop constructs'
		
		// for loop
		// for ( <initialisation> ; <test condition>; <afterthought> )
		// the initialisation is run only once, that's before the loop starts spinning,
		// the test condition is executed before each loop execution, when unsatisfied,
		// which means when it is false, loop terminates
		// afterthought bit executed when the loop completes each cycle
		// As a rule of thumb, for loops are preferred when the iteration count
		// is known before hand
		
		boolean sillyCondition = false;
		boolean anotherCondition = false;
		int i;
		int j = 1;
		for (i = 0; i < 3; i++) {
			System.out.print(" i:" + i);
			
			if (sillyCondition == true) {
				// sometimes we might want to terminate the loop
				// before the 'test condition' is false
				// this is what break does
				break;
			}
			
			if (anotherCondition == false) {
				// sometimes we want to skip the rest of the code in the 
				// loop body and this is what continue does
				// it goes to the } of the loop, then continues with the
				// afterthought and test condition as normal
				continue;
			}
			j *= (i + 1);
		}
		// in the loop, the last value of 'i' is 2, then i++ gets it to 3,
		// 3 < 3 evaluates to false, so, when we exit the loop, 'i' is 3
		// also note that '\n' in " "s gives us a new line, '\t' inserts a TAB
		System.out.println("\ni after loop: " + i + " .\tSee how it is incremented again");
		
		// 'while' and its sister 'do-while'
		// 'while' loops as long as its condition is true or until
		// 'break' executed in it. This is useful when we are waiting
		// for a certain thing to happen and have no idea how many tries
		// it will take, ie searching for a particular word in a file
		// Note that the body of 'while' may never get executed,
		// if the 'condition' is false straight off the bat, for example,
		// the file is empty
		i = 0;
		while(i == 0) {
			// let's assume this is set based on an external condition like
			// we turn the heater on and wait for the temperature to go above a 
			// threshold
			i = 1;
			System.out.println("Please be patient while we 'while'");
		}
		
		// 'do-while' is almost identical, with the only difference of
		// condition being checked at the end. This guarantees at least
		// one run of the loop. This is better suited for situations like
		// user-input reading and repeating if the value entered is invalid
		// note the ';' after while
		do {
			System.out.println("Now we are in do-while, for a while");
		} while (i == 0);
		
		System.out.println("");
	}
	
	public static void someConditionalOps() {
		// in programming it is quite common to test various conditions and 
		// combine multiple conditions together
		// so, if you need to identify 55-60 year old smoker males
		// age >= 55 && age <= 60 && smoker == true && gender == 'm'
		// you recognise >/< signs, when they are followed by =, it means
		// greater/less than or equal
		// '==' checks for equality of 'primitives' . do not confuse it with
		// single '=' which is used for assignment. Also, do not get too used to
		// using == in Java for comparison as '.equals()' method is to come later
		// finally, && is the logical AND operation, which requires both sides
		// of the conditional to be true
		// during the left to right evaluation of &&s, the first 'false' condition
		// stops the evaluation of rest, it will stop after '3 > 5' test which 
		// evaluates to 'false'
		// 65 == 65 && 3 > 5 && 2 < 5
		
		// we can also use logical OR, ||, to combine multiple conditionals
		// in logical OR, one 'true' is sufficient to make the whole thing true,
		// so, it will stop evaluating if/when it finds the first 'true'
		// this stopping and not processing the rest behaviour is called 'short circuiting'
		// && and || are short circuit operators
		// this might seem trivial now but when you start using method calls in conditionals
		// it will become more important
		
		// if-else if-else is a common construct to use conditionals
		// you can use the 'if' on its own or combine it with an 'else'
		// to fallback to when the 'if' condition is 'false' or you can add
		// as many as 'else if's to be evaluated in order until a 'true' is found
		int age = 70;
		byte gender = 'f';
		if (age >= 55 && gender == 'm') {
			System.out.println("if-else if-else point 1");
		} else if (gender == 'm' || age < 40) {
			System.out.println("if-else if-else point 2");
		} else {
			System.out.println("if-else if-else point 3");
		}
		
		System.out.println("");
	}
	
	public static void someKeyboardInput() {
		int userInput = -1;
		
		// This is just a print, Java doesn't care what you 'say'
		System.out.println("Enter a whole number (0-100): ");
		
		// This line instantiates Scanner with System.in, which is the keyboard input
		// so, this 'Scanner' object, 'scans' input from the keyboard
		// Also don't forget to put import java.util.Scannenr; at the top
		Scanner sc = new Scanner(System.in);
		
		// it is specifically waiting for an int, we are blocked here until 'enter key'
		// is pressed
		if (sc.hasNextInt()) {
			// if it was an 'int', 'hastNextInt' returns true and we read the value by
			// nextInt, otherwise, skip the 'if body' and move on
			userInput = sc.nextInt();
		}
		
		if (userInput >= 0 && userInput <= 100) {
			System.out.println("Thanks for entering " + userInput);
		} else {
			System.out.println("I don't like what you enter :(");
		}
		
		System.out.println("");
	}
	
	public static void someMaths() {
		// these expressions are evaluated from left to write and according to BODMAS rule
		// Bracket(Parentheses) Order(Like power or root) Division Multiplication Addition Subtraction
		
		// ok, we go left to right and search BODMAS
		// no Brackets or Power but we find Divisions and let's do them first,
		// as both 4 and 5 are whole numbers, the result of 4 / 5
		// is also evaluated as whole number and it is 0 instead of 0.8
		// simimlary 1 / 5 is 0 too, then we have 1 + 0 + 0
		int i = 1 + 4 / 5 +  1 / 5;
		System.out.println("i1: " + i);
		
		// Now, let's change some types to 'float' by adding an 'f' after the numbers
		// This makes results to be stored as 'float' too
		// Then we have 1 + 0.8 + 0.2 + 0.1 = 2.1
		// However putting a type between brackets is called 'type casting'
		// and here, we 'downcast' decimal number to a whole number
		// by doing (int)(2.1f), which gives us 2
		// So, we used decimals all the way and dropped it in the end
		i = (int)(1 + 4f / 5 +  1f / 5 + 0.1f);
		System.out.println("i2: " + i);

		// if you want to store decimals, use float or double
		// An interesting fact, as 'float's require some apprximation
		// same calculations on different JVMs might produce slightly different
		// 'float' results, unless you use 'strict floating point', don't worry
		// about it though, just mentioning
		float f = (1 + 4f / 5 +  1f / 5 + 0.1f);
		System.out.println("f: " + f);
		
		System.out.println("");
	}
	
	public static void someCollections() {
		// This is the array notation. Arrays are same type of consecutive blocks
		// in memory. Below, we have an 'int' array variable which holds reference
		// to a memory block of 5 ints.
		int[] primIntArr = new int[5];
		
		// array 'index' starts from 0 and goes to length - 1. 
		// so for the array above, we have indices 0 to 4.
		// primIntArr.length gives us the length of the array
		primIntArr[0] = 9;
		
		for (int i = 0; i < primIntArr.length; ++i) {
			// do something with the array elements, like
			// increment each element by one
			primIntArr[i] = primIntArr[i] + 1;
			
			// if you are going to add something to itself, you can use the
			// x += n; notation, which mean x = x + n
			// primIntArr[i] += 1;
			// if 'n' is one, then we have x++; which is again x = x + 1
			// x += n works for -, /, * and even 'bitwise operators' (can ignore for now)
			// we also have -- and these 'double +/-' signs can come before or after the
			// variable name as in ++x, x++, --x, x-- . Pre and post operations are slightly
			// different but again, don't worry too much about it now.
			// primIntArr[i]++;
		}
		
		// Java evolved over time, so, we can iterate less painfully too
		// This just says, loop through elements of primIntArr and assign it to i,
		// one at a time of course
		// This type of 'for loop' is called an 'enhanced for loop'
		System.out.print("primIntArr: ");
		for (int i : primIntArr) {
			System.out.print(i + " ");
		}
		
		System.out.println();
		
		// Arrays are a bit annoying though. We can change what's assigned
		// to each cell but we can't grow the array beyond the initial size
		// , 5, in this example. So, what do we do? Use collections.
		// Java offers a generous selection of 'collection's for different needs
		// These needs are to be covered in Data Structures parts of the module
		// For example, ArrayList is like an array but it can grow or shrink too
		// Also don't forget to put import java.util.Arraylist; at the top
		// Do you also notice how ArrayList starts with a capital, which is a hint
		// that it is a 'Class'? . Also, as ArrayList is a Class, we use the methods 
		// of ArrayList to add/remove elements to it, like '.add(5)'
		// The type of the elements go between <>s. So,
		// ArrayList<Integer> only stores Integer elements and other types won't be
		// allowed. You can instantiate an ArrayList which doesn't care about the type
		// but normally you shouldn't be doing that.
		ArrayList<Integer> intArrList = new ArrayList<>();
		System.out.println("intArrList size: " + intArrList.size());
		intArrList.add(3);
		intArrList.add(8);
		System.out.println("intArrList size: " + intArrList.size());
		
		System.out.println("Let's loop for-each site, no need to mess with old school for loops");
		System.out.println("This works for [] arrays too");
		for (int val : intArrList) {
			System.out.println(val);
		}
		
		System.out.println("");
	}
}
