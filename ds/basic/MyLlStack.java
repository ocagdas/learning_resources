
public class MyLlStack<E> {
    @SuppressWarnings("unchecked")
	public MyLlStack(int capacityIn) {
    	internalLl = new MyLlSentinel<>();
    	capacity = capacityIn;
    }
    
    void display() {
    	internalLl.display();
    }
    
    boolean push(E val) {
    	//BIGO: O(1)
    	if (size < capacity) {
    		++size;
    		internalLl.add(val);
    		
    		return true;
    	} else {
    		return false;
    	}
    }
    
    E pop() {
    	if (size > 0) {
    		--size;
	    	E retVal = internalLl.front();
	    	internalLl.remove(retVal);
	    	
	    	return retVal;
    	} else {
    		return null;
    	}
    }
    
    boolean empty() {
    	return size == 0;
    }
    
    boolean contains(E val) {
    	return internalLl.contains(val);
    }
    
    private MyLlSentinel<E> internalLl;
    private int capacity;
    private int size;
}
