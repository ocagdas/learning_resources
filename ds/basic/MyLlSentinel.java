
public class MyLlSentinel<E> {
	// inner class to represent the LL node
	private static final class LlNode<E> {
		// normally these members should be private and accessed
		// via getters/setters, but as this is just an exercise,
		// make them available to the outer class, MyLl
		E data;
		LlNode<E> next;
		LlNode<E> prev;
		
		LlNode(E dataIn) {
			data = dataIn;
		}
	}
	
	private LlNode<E> first_sent;
	private LlNode<E> last_sent;
	private int size;
	
	MyLlSentinel() {
		first_sent = new LlNode<>(null);
		last_sent = new LlNode<>(null);
		first_sent.next = last_sent;
		last_sent.prev = first_sent;
	}
	
	void add(E val) {
		//BIGO: O(1)
		LlNode<E> newNode = new LlNode<>(val);
		newNode.next = first_sent.next;
		newNode.prev = first_sent;
		first_sent.next.prev = newNode;
		first_sent.next = newNode;
		++size;
	}
	
	boolean remove(E val) {
		boolean found = false;
		//BIGO: O(n)
		LlNode<E> node = find(val);
		
		if (node != null) {
			found = true;
			--size;
			// use of sentinel nodes remove the checks required otherwise
			node.prev.next = node.next;
			node.next.prev = node.prev;
		}
		
		return found;
	}
	
	E front() {
		return first_sent.next.data;
	}
	
	boolean contains(E val) {
		LlNode<E> node = find(val);
		
		return null != node;
	}
	
	void display() {
		LlNode<E> node = first_sent.next;
		
		if (size > 0) {
			while (last_sent != node) {
				System.out.print(node.data + " ");
				node = node.next;
			}
			System.out.println();
		} else {
			System.out.println("List empty");
		}
	}
	
	private LlNode<E> find(E val) {
		//BIGO: O(n)
		
		if (null != val) {
			// backup last sentinel's data, which should be null anyway
			// override last_sent's data with search key, so, we can loop with only one check
			LlNode<E> node = first_sent.next;
			E lastSentDataBck = last_sent.data;
			last_sent.data = val;
			
			// we'll either stop when we find the key in the list
			// or when we get the hit on last_sent's data, which indicates end-of-list
			// see how simpler this loop is compared to without-sentinel impl and also
			// how it cuts the 'branch' ops by half
			while (false == node.data.equals(val)) {
				node = node.next;
			}
			
			if (last_sent == node) {
				node = null;
			}
			last_sent.data = lastSentDataBck;
			
			return node;
		} else {
			return null;
		}
	}

}
