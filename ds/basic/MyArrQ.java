
public class MyArrQ<E> {
    @SuppressWarnings("unchecked")
	public MyArrQ(int capacityIn) {
    	capacity = capacityIn;
    	size = 0;
        qArr = (E[]) new  Object[capacity];
        head = tail = 0;
    }
    
    boolean isEmpty() {
        return head == tail;
    }
    
    void display() {
    	if (size > 0) {
    		//BIGO: O(n)
    		int i = 0;
    		while (i < size) {
    			System.out.print(qArr[(i + head) % capacity] + " ");
    			++i;
    		}
    		System.out.println();
    	} else {
    		System.out.println("Empty");
    	}
    }
    
    boolean enqueue(E val) {
        if (size < capacity) {
        	//BIGO: O(1)
        	++size;
            qArr[tail] = val;
            tail = (tail + 1) % capacity;
        } else {
            return false;
        }
        
        return true;
    }
    
    E dequeue() {
        if (0 == size) {
            return null;
        }
        
        //BIGO: O(1)
        --size;
        E val = qArr[head];
        head = (head + 1) % capacity;
        
        return val;
    }
    
    boolean contains(E val) {
    	int i = 0;
    	
    	while (false == qArr[(i++ + head) % capacity].equals(val)) {
    		if (i >= size) {
    			i = -1;
    			break;
    		}
    	}
    	
    	return i > 0;
    }
    
    private E[] qArr;
    private int head;
    private int tail;
    private int capacity;
    private int size;
}

