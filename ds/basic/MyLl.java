
public class MyLl<E> {
	// inner class to represent the LL node
	private static final class LlNode<E> {
		// normally these members should be private and accessed
		// via getters/setters, but as this is just an exercise,
		// make them available to the outer class, MyLl
		E data;
		LlNode<E> next;
		LlNode<E> prev;
		
		LlNode(E dataIn) {
			data = dataIn;
		}
	}
	
	private LlNode<E> first;
	private LlNode<E> last;
	private int size;
	
	MyLl() {
	}
	
	void add(E val) {
		//BIGO: O(1)
		LlNode<E> newNode = new LlNode<>(val);
		if (null == first) {
			first = newNode;
			last = newNode;
		} else {
			newNode.next = first;
			first.prev = newNode;
			first = newNode;
		}
		++size;
	}
	
	boolean remove(E val) {
		boolean found = false;
		//BIGO: O(n)
		LlNode<E> node = find(val);
		
		//BIGO: O(1)
		// after locating the element, actual remove is O(1)
		if (node != null) {
			found = true;
			--size;
			if (node.prev != null) {
				node.prev.next = node.next;
			} else {
				first = node.next;
			}
			
			if (node.next != null) {
				node.next.prev = node.prev;
			} else {
				last = node.prev;
			}
		}
		
		return found;
	}
	
	boolean contains(E val) {
		boolean found = false;
		LlNode<E> node = find(val);
		
		if (null != node) {
			found = true;
		}
		
		return found;
	}
	
	void display() {
		LlNode<E> node = first;
		
		if (size > 0) {
		while (null != node) {
				System.out.print(node.data + " ");
				node = node.next;
			}
			System.out.println();
		} else {
			System.out.println("List empty");
		}
	}
	
	private LlNode<E> find(E val) {
		//BIGO: O(n)
		LlNode<E> node = first;
		
		while (node != null) {
			if (node.data.equals(val)) {
				break;
			}
			node = node.next;
		}
		
		return node;
	}

}
